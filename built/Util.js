"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var Stack = /** @class */ (function () {
    function Stack() {
        this.stack = [];
    }
    Stack.prototype.pop = function () {
        return this.stack.pop();
    };
    Stack.prototype.pop2 = function () {
        if (this.length(2))
            return [this.stack.pop(), this.stack.pop()];
        return undefined;
    };
    Stack.prototype.push = function (element) {
        this.stack.push(element);
        return this;
    };
    Stack.prototype.top = function () {
        return this.stack[this.stack.length - 1];
    };
    Stack.prototype.length = function (length) {
        if (length)
            return this.stack.length >= length;
        return this.stack.length;
    };
    /** For Debugging purposes */
    Stack.prototype.print = function (stackName) {
        console.log((stackName || 'Stack') + ": " + JSON.stringify(this.stack, null, 2));
    };
    return Stack;
}());
exports.Stack = Stack;
function operand(string) {
    if (/^(([a-zA-Z])|(-?\d+))$/.test(string))
        return /^[a-zA-Z]+$/.test(string) ? 'VARIABLE' : 'NUMBER';
    return null;
}
exports.operand = operand;
function operation(string) {
    var operations = new Map()
        .set('+', 'PLUS')
        .set('-', 'MINUS')
        .set('*', 'MULT')
        .set('/', 'DIV')
        .set('^', 'POW')
        .set('**', 'POW')
        .set('!', 'FAC');
    return operations.get(string) || null;
}
exports.operation = operation;
function notEnoughArguments(operation, expected, got) {
    console.error("Not enough arguments for operation `" + operation + "`; expected " + expected + ", got " + got);
    process.exit(1);
}
exports.notEnoughArguments = notEnoughArguments;
function factorial(n) {
    if (n < 0)
        return -1;
    if (n == 0)
        return 1;
    return n * factorial(n - 1);
}
exports.factorial = factorial;
