"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var Util_1 = require("./Util");
var args = process.argv[process.argv.length - 1].split(' ');
var stack = new Util_1.Stack();
for (var _i = 0, args_1 = args; _i < args_1.length; _i++) {
    var arg = args_1[_i];
    if (Util_1.operand(arg) == 'NUMBER')
        stack.push(arg);
    else if (Util_1.operand(arg) == 'VARIABLE') {
        console.error('Variables are not implemented yet');
        process.exit(1);
    }
    else if (Util_1.operation(arg)) {
        var op = Util_1.operation(arg);
        switch (op) {
            // PLUS operation
            case 'PLUS': {
                if (!stack.length(2))
                    Util_1.notEnoughArguments('PLUS', 2, stack.length());
                var OPS = stack.pop2();
                stack.push((parseFloat(OPS[0]) + parseFloat(OPS[1])).toString());
                break;
            }
            // MINUS operation
            case 'MINUS': {
                if (!stack.length(2))
                    Util_1.notEnoughArguments('MINUS', 2, stack.length());
                var OPS = stack.pop2();
                stack.push((parseFloat(OPS[1]) - parseFloat(OPS[0])).toString());
                break;
            }
            // MULT[IPLY] operation
            case 'MULT': {
                if (!stack.length(2))
                    Util_1.notEnoughArguments('MULT', 2, stack.length());
                var OPS = stack.pop2();
                stack.push((parseFloat(OPS[0]) * parseFloat(OPS[1])).toString());
                break;
            }
            // DIV[IDE] operation
            case 'DIV': {
                if (!stack.length(2))
                    Util_1.notEnoughArguments('DIV', 2, stack.length());
                var OPS = stack.pop2();
                stack.push((parseFloat(OPS[1]) / parseFloat(OPS[0])).toString());
                break;
            }
            // POW[ER] operation
            case 'POW': {
                if (!stack.length(2))
                    Util_1.notEnoughArguments('POW', 2, stack.length());
                var OPS = stack.pop2();
                stack.push((Math.pow(parseFloat(OPS[1]), parseFloat(OPS[0]))).toString());
                break;
            }
            // FAC[TORIAL] operation
            case 'FAC': {
                if (!stack.length(1))
                    Util_1.notEnoughArguments('FAC', 1, stack.length());
                var OP = stack.pop();
                var res = Util_1.factorial(parseInt(OP));
                if (res < 1) {
                    console.error("Invalid value `" + OP + "` for factorial");
                    process.exit();
                }
                stack.push(res.toString());
            }
        }
    }
    else {
        console.error("Unrecognisable word " + arg);
        process.exit(1);
    }
}
if (stack.length(2)) {
    console.error("Unused data on the stack; found " + (stack.length() - 1) + " too many elements");
    process.exit(1);
}
else
    console.log(stack.pop());
