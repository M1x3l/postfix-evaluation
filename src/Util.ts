export type Operation =
	| 'PLUS'
	| 'MINUS'
	| 'MULT'
	| 'DIV'
	| 'POW'
	| 'POW'
	| 'FAC';

export class Stack {
	private stack: string[] = [];

	pop() {
		return this.stack.pop();
	}

	pop2() {
		if (this.length(2)) return [this.stack.pop()!, this.stack.pop()!];
		return undefined;
	}

	push(element: string) {
		this.stack.push(element);
		return this;
	}

	top() {
		return this.stack[this.stack.length - 1];
	}

	length(length?: number) {
		if (length) return this.stack.length >= length;
		return this.stack.length;
	}

	/** For Debugging purposes */
	print(stackName?: string) {
		console.log(
			`${stackName || 'Stack'}: ${JSON.stringify(this.stack, null, 2)}`
		);
	}
}

export function operand(string: string) {
	if (/^(([a-zA-Z])|(-?\d+))$/.test(string))
		return /^[a-zA-Z]+$/.test(string) ? 'VARIABLE' : 'NUMBER';
	return null;
}

export function operation(string: string): Operation | null {
	const operations = new Map()
		.set('+', 'PLUS')
		.set('-', 'MINUS')
		.set('*', 'MULT')
		.set('/', 'DIV')
		.set('^', 'POW')
		.set('**', 'POW')
		.set('!', 'FAC');
	return operations.get(string) || null;
}

export function notEnoughArguments(
	operation: Operation,
	expected: number,
	got: number
) {
	console.error(
		`Not enough arguments for operation \`${operation}\`; expected ${expected}, got ${got}`
	);
	process.exit(1);
}

export function factorial(n: number): number {
	if (n < 0) return -1;
	if (n == 0) return 1;
	return n * factorial(n - 1);
}
