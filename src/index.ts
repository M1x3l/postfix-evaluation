import {
	factorial,
	notEnoughArguments,
	operand,
	operation,
	Operation,
	Stack,
} from './Util';

const args = process.argv[process.argv.length - 1].split(' ');

const stack = new Stack();

for (let arg of args) {
	if (operand(arg) == 'NUMBER') stack.push(arg);
	else if (operand(arg) == 'VARIABLE') {
		console.error('Variables are not implemented yet');
		process.exit(1);
	} else if (operation(arg)) {
		const op = operation(arg);

		switch (op) {
			// PLUS operation
			case 'PLUS': {
				if (!stack.length(2))
					notEnoughArguments('PLUS', 2, stack.length() as number);
				const OPS = stack.pop2()!;
				stack.push((parseFloat(OPS[0]) + parseFloat(OPS[1])).toString());
				break;
			}
			// MINUS operation
			case 'MINUS': {
				if (!stack.length(2))
					notEnoughArguments('MINUS', 2, stack.length() as number);
				const OPS = stack.pop2()!;
				stack.push((parseFloat(OPS[1]) - parseFloat(OPS[0])).toString());
				break;
			}
			// MULT[IPLY] operation
			case 'MULT': {
				if (!stack.length(2))
					notEnoughArguments('MULT', 2, stack.length() as number);
				const OPS = stack.pop2()!;
				stack.push((parseFloat(OPS[0]) * parseFloat(OPS[1])).toString());
				break;
			}
			// DIV[IDE] operation
			case 'DIV': {
				if (!stack.length(2))
					notEnoughArguments('DIV', 2, stack.length() as number);
				const OPS = stack.pop2()!;
				stack.push((parseFloat(OPS[1]) / parseFloat(OPS[0])).toString());
				break;
			}
			// POW[ER] operation
			case 'POW': {
				if (!stack.length(2))
					notEnoughArguments('POW', 2, stack.length() as number);
				const OPS = stack.pop2()!;
				stack.push((parseFloat(OPS[1]) ** parseFloat(OPS[0])).toString());
				break;
			}
			// FAC[TORIAL] operation
			case 'FAC': {
				if (!stack.length(1))
					notEnoughArguments('FAC', 1, stack.length() as number);
				const OP = stack.pop()!;
				const res = factorial(parseInt(OP));
				if (res < 1) {
					console.error(`Invalid value \`${OP}\` for factorial`);
					process.exit();
				}
				stack.push(res.toString());
			}
		}
	} else {
		console.error(`Unrecognisable word ${arg}`);
		process.exit(1);
	}
}

if (stack.length(2)) {
	console.error(
		`Unused data on the stack; found ${
			(stack.length() as number) - 1
		} too many elements`
	);
	process.exit(1);
} else console.log(stack.pop());
